//Edward Esposito and Marjan Atienza
package com.example.photoapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import java.util.ArrayList;

public class Helper {

    public ArrayList<String> tagList = new ArrayList<String>();

    public void infoAlert(Context context, String Title, String Message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(Title);
        alert.setMessage(Message);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;
            }
        });
        alert.show();
    }

    public void msgAlert(Context context, String Title, String Message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(Title);
        alert.setMessage(Message);
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;
            }
        });
        alert.show();
        return;
    }

    public boolean checkIfTagExists(String sTag) {
        // if not exists add to taglist
        for (String hcTag : tagList){
            if(hcTag.trim().equalsIgnoreCase(sTag.trim()))
                return true;
        }
        return false;
    }

}
