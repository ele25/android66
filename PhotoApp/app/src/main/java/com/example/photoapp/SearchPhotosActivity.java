//Edward Esposito and Marjan Atienza
package com.example.photoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class SearchPhotosActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.photoapp.MESSAGE";
    private Helper hc = new Helper();
    public static AllAlbums myAlbums;
    final Context context = this;
    public ListView searchNames;
    public PhotoAdapter searchAdapter;
    public Photo curPhoto;
    String curSpinnerTag1;
    String curSpinnerTag2;
    String curSpinnerConjunction;
    public ArrayList<Photo> searchPhotos = new ArrayList<Photo>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        myAlbums = AllAlbums.readData(this);
        if(myAlbums == null) {
            myAlbums = new AllAlbums();
        }

        if (myAlbums.albums == null) {
            myAlbums.albums = new ArrayList<Album>();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_photos);

        loadAllSpinners();

        searchNames = (ListView)findViewById(R.id.lstPhotoSearch);
        searchNames.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        searchNames.setSelection(0);

        searchAdapter = new PhotoAdapter(this, searchPhotos);
        searchAdapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
        searchNames.setAdapter(searchAdapter);

        if (searchPhotos.size() > 0) {
            curPhoto = (Photo) (searchNames.getItemAtPosition(0));

//            ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
//            curImage.setImageBitmap(curPhoto.getBitmap());
        }

        searchNames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(curPhoto == null) {
                    curPhoto = (Photo) (searchNames.getItemAtPosition(0));
//                    ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
//                    curImage.setImageBitmap(curPhoto.getBitmap());
                    searchNames.setSelection(0);
                } else {
                    curPhoto = (Photo) (searchNames.getItemAtPosition(position));
//                    ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
//                    curImage.setImageBitmap(curPhoto.getBitmap());
                }
            }});

    }

    private void loadAllSpinners() {

        List<String> listSpinnerTag1 = new ArrayList<String>();
        listSpinnerTag1.add("location");
        listSpinnerTag1.add("position");
        Spinner sp1 = (Spinner) findViewById(R.id.spinTag1);

        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, listSpinnerTag1);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(adp1);

        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
//                Toast.makeText(getBaseContext(), listSpinnerTag1.get(position), Toast.LENGTH_SHORT).show();
                curSpinnerTag1 = listSpinnerTag1.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
//                Toast.makeText(getBaseContext(), "NoHo" + listSpinnerTag1.get(0), Toast.LENGTH_SHORT).show();
                curSpinnerTag1 = listSpinnerTag1.get(0);
            }
        });


        List<String> listSpinnerTag2 = new ArrayList<String>();
        listSpinnerTag2.add("location");
        listSpinnerTag2.add("position");
        Spinner sp2 = (Spinner) findViewById(R.id.spinTag2);

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, listSpinnerTag2);
        adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp2.setAdapter(adp2);

        sp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
//                Toast.makeText(getBaseContext(), listSpinnerTag2.get(position), Toast.LENGTH_SHORT).show();
                curSpinnerTag2 = listSpinnerTag2.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
//                Toast.makeText(getBaseContext(), "NoHo" + listSpinnerTag2.get(0), Toast.LENGTH_SHORT).show();
                curSpinnerTag2 = listSpinnerTag2.get(0);
            }
        });


        List<String> listSpinnerConjunction = new ArrayList<String>();
        listSpinnerConjunction.add("AND");
        listSpinnerConjunction.add("OR");
        Spinner sp3 = (Spinner) findViewById(R.id.spinConjunction);

        ArrayAdapter<String> adp3 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, listSpinnerConjunction);
        adp3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp3.setAdapter(adp3);

        sp3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
//                Toast.makeText(getBaseContext(), listSpinnerConjunction.get(position), Toast.LENGTH_SHORT).show();
                curSpinnerConjunction = listSpinnerConjunction.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
//                Toast.makeText(getBaseContext(), "NoHo" + listSpinnerConjunction.get(0), Toast.LENGTH_SHORT).show();
                curSpinnerConjunction = listSpinnerConjunction.get(0);
            }
        });
    }

    public void searchPhotosByTag(View view) {

        EditText editText = (EditText) findViewById(R.id.edTagValue1);
        String message = editText.getText().toString().trim();
        if(message.equals("")) {
            hc.infoAlert(context, "Search", "Please enter an tag to search for.");
        }

        getPhotosByTag();

    }

    public boolean checkIfPhotoExists(Photo curPhoto) {
        // if not exists add to searchPhotos
        for (Photo sPhoto : searchPhotos){
//			System.out.println("Photo: " +  sPhoto);
            if(sPhoto.getName().equalsIgnoreCase(curPhoto.getName()))
                return true;
        }
        return false;
    }

    public void getPhotosByTag() {
        searchPhotos.clear();
//        searchAdapter.clear();
        boolean bTag1 = false;
        boolean bTag2 = false;

        EditText editText1 = (EditText) findViewById(R.id.edTagValue1);
        String tagVal1 = editText1.getText().toString().trim();
        EditText editText2 = (EditText) findViewById(R.id.edTagValue2);
        String tagVal2 = editText2.getText().toString().trim();

        //single tag search
        if(tagVal2.equals("")) {
                for (Album cAlbum : myAlbums.albums) {
//					System.out.println("Album: " +  cAlbum.getName());
                    for (Photo cPhoto : cAlbum.getPhotos()) {
//						System.out.println("Photo: " +  cPhoto.getName());
                        for (Tag cTag : cPhoto.getTags()) {
                            if((cTag.getName().equalsIgnoreCase(curSpinnerTag1)) && (cTag.getValue().startsWith(tagVal1))) {
                            //if((cTag.getName().equalsIgnoreCase(curSpinnerTag1)) && (cTag.getValue().equalsIgnoreCase(tagVal1))) {
                                    // if not exists add to searchPhotos
                                if(!checkIfPhotoExists(cPhoto)) {
                                    searchPhotos.add(cPhoto);
                                }
                            }
                        }
                    }
                }
        }

        //double tag search OR
        if((!tagVal1.equals("")) && (!tagVal2.equals("")) && (curSpinnerConjunction.equals("OR"))) {
                for (Album cAlbum : myAlbums.albums) {
//					System.out.println("Album: " +  cAlbum.getName());
                    for (Photo cPhoto : cAlbum.getPhotos()) {
//						System.out.println("Photo: " +  cPhoto.getName());
                        for (Tag cTag : cPhoto.getTags()) {
                            if((cTag.getName().equalsIgnoreCase(curSpinnerTag1)) && (cTag.getValue().startsWith(tagVal1))
                                    || (cTag.getName().equalsIgnoreCase(curSpinnerTag2)) && (cTag.getValue().startsWith(tagVal2))) {
//                            if((cTag.getName().equalsIgnoreCase(curSpinnerTag1)) && (cTag.getValue().equalsIgnoreCase(tagVal1))
//                                    || (cTag.getName().equalsIgnoreCase(curSpinnerTag2)) && (cTag.getValue().equalsIgnoreCase(tagVal2))) {
                                // if not exists add to searchPhotos
                                if(!checkIfPhotoExists(cPhoto)) {
                                    searchPhotos.add(cPhoto);
                                }
                            }
                        }
                    }
                }
        }

        //double tag search AND
        if((!tagVal1.equals("")) && (!tagVal2.equals("")) && (curSpinnerConjunction.equals("AND"))) {
                for (Album cAlbum : myAlbums.albums) {
//					System.out.println("Album: " +  cAlbum.getName());
                    for (Photo cPhoto : cAlbum.getPhotos()) {
//						System.out.println("Photo: " +  cPhoto.getName());
                        for (Tag cTag : cPhoto.getTags()) {
                            if((cTag.getName().equalsIgnoreCase(curSpinnerTag1)) && (cTag.getValue().startsWith(tagVal1))) {
//                            if((cTag.getName().equalsIgnoreCase(curSpinnerTag1)) && (cTag.getValue().equalsIgnoreCase(tagVal1))) {
                                bTag1 = true;
                            }
                            if((cTag.getName().equalsIgnoreCase(curSpinnerTag2)) && (cTag.getValue().startsWith(tagVal2))) {
//                            if((cTag.getName().equalsIgnoreCase(curSpinnerTag2)) && (cTag.getValue().equalsIgnoreCase(tagVal2))) {
                                bTag2 = true;
                            }
                        }
                        if((bTag1) && (bTag2)) {
                            // if not exists add to searchPhotos
                            if(!checkIfPhotoExists(cPhoto)) {
                                searchPhotos.add(cPhoto);
                            }
                        }
                        bTag1 = false;
                        bTag2 = false;
                    }
                }
        }

        if(searchPhotos.size() == 0)  {
            searchPhotos.clear();
            searchAdapter.clear();
            return;
        }
        searchAdapter = new PhotoAdapter(this, searchPhotos);
        searchAdapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
        searchNames.setAdapter(searchAdapter);

        if (searchPhotos.size() > 0) {
            curPhoto = (Photo) (searchNames.getItemAtPosition(0));
        }

        return;
    }

}