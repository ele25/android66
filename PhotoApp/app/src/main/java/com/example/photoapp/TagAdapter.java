//Edward Esposito and Marjan Atienza
package com.example.photoapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class TagAdapter extends ArrayAdapter<Tag>{
    private Helper hc = new Helper();
    Context context;
    String value;

    public TagAdapter(Context context, ArrayList<Tag> tags) {
        super(context, 0, tags);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Tag tag = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_listview_photo, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvTagName);
        // Populate the data into the template view using the data object
        tvName.setText(tag.getName() + " - " + tag.getValue());

//        ImageView iconPhoto = (ImageView) convertView.findViewById(R.id.iconPhoto);
//        iconPhoto.setImageBitmap(photo.getBitmap());

        tvName.setTag(position);
        // Attach the click event handler
//        tvName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                int position = (Integer) view.getTag();
//                Tag tag = getItem(position);
//            }
//        });
//        Toast.makeText(context, value, Toast.LENGTH_SHORT).show();
        // Return the completed view to render on screen
        return convertView;
    }

}
