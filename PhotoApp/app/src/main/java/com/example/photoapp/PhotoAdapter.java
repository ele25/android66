//Edward Esposito and Marjan Atienza
package com.example.photoapp;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class PhotoAdapter extends ArrayAdapter<Photo>{
    private Helper hc = new Helper();

    public PhotoAdapter(Context context, ArrayList<Photo> photos) {
        super(context, 0, photos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Photo photo = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_listviev_image, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        // Populate the data into the template view using the data object
        tvName.setText(photo.getName());

        ImageView iconPhoto = (ImageView) convertView.findViewById(R.id.iconPhoto);
        iconPhoto.setImageBitmap(photo.getBitmap());

        tvName.setTag(position);
        // Attach the click event handler
//        tvName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                int position = (Integer) view.getTag();
//                // Access the row position here to get the correct data item
//                Photo photo = getItem(position);
//                //hc.infoAlert(getContext(), "Albums List", album.getName());
//            }
//        });

        // Return the completed view to render on screen
        return convertView;
    }


}
