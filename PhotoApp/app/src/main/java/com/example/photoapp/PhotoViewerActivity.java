//Edward Esposito and Marjan Atienza
package com.example.photoapp;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class PhotoViewerActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.photoapp.MESSAGE";
    public static final String albumName = "album";
    private Helper hc = new Helper();
    public static AllAlbums myAlbums;
    public Album curAlbum;
    final Context context = this;
    public ListView tagNames;
    public TagAdapter tagAdapter;
    public Photo curPhoto;
    public Tag curTag;
    public ImageView curImage;
    String curSpinnerValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        myAlbums = AllAlbums.readData(this);
        if(myAlbums == null) {
            myAlbums = new AllAlbums();
        }

        if (myAlbums.albums == null) {
            myAlbums.albums = new ArrayList<Album>();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);

//        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
//            @Override
//            public void handleOnBackPressed() {
//                // Handle the back button event
//                hc.infoAlert(context, "Back", "Back to Album.");
//            }
//        };
//        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);


        Intent intent = getIntent();
        String message = intent.getStringExtra(AlbumPhotosActivity.EXTRA_MESSAGE);
        TextView textView = findViewById(R.id.tvPhotoViewer);
        textView.setText(message);

        String albumName = intent.getStringExtra(AlbumPhotosActivity.albumName);
        curAlbum = myAlbums.getAlbumByName(albumName);

        String photoName = intent.getStringExtra(AlbumPhotosActivity.photoName);
        curPhoto = getPhotoByName(curAlbum, photoName);

        loadSpinner();

        tagNames = (ListView)findViewById(R.id.lstTags);
        tagNames.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        tagNames.setSelection(0);

        tagAdapter = new TagAdapter(this, curPhoto.getTags());
        tagAdapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName())==0 ?
                a.getValue().compareToIgnoreCase(b.getValue()) :
                a.getName().compareToIgnoreCase(b.getName()));
        tagNames.setAdapter(tagAdapter);

        if (curPhoto.getTags().size() > 0) {
            curTag = (Tag) (tagNames.getItemAtPosition(0));
        }

        ImageView curImage = (ImageView) findViewById(R.id.imgPhotoView);
        curImage.setImageBitmap(curPhoto.getBitmap());

        tagNames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(curTag == null) {
                    curTag = (Tag) (tagNames.getItemAtPosition(0));
//                    ImageView curImage = (ImageView) findViewById(R.id.imgPhotoView);
//                    curImage.setImageBitmap(curPhoto.getBitmap());
                    tagNames.setSelection(0);
                } else {
                    curTag = (Tag) (tagNames.getItemAtPosition(position));
                    ImageView curImage = (ImageView) findViewById(R.id.imgPhotoView);
                    curImage.setImageBitmap(curPhoto.getBitmap());
                }
            }});

    }

    @Override
    public void onBackPressed() {
//        hc.infoAlert(context,"Back","Back Pressed.");
        //goback to AlbumPhotosActivity
        Intent intent = new Intent(this, AlbumPhotosActivity.class);
        intent.putExtra(EXTRA_MESSAGE, "Album: " + curAlbum.getName());

        intent.putExtra(albumName, curAlbum.getName());
        startActivity(intent);
    }

    //handle up button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, AlbumPhotosActivity.class);
        intent.putExtra(EXTRA_MESSAGE, "Album: " + curAlbum.getName());

        intent.putExtra(albumName, curAlbum.getName());
        startActivity(intent);
        return true;
    }

    public Photo getPhotoByName(Album oAlbum, String sPhotoName) {
        for (Photo pPhoto : oAlbum.getPhotos()) {
            if (pPhoto.getName().trim().toLowerCase().equals(sPhotoName.trim().toLowerCase())) {
                return pPhoto;
            }
        }
        return null;
    }

    public void addTag(View view) {

        EditText editText = (EditText) findViewById(R.id.etTagValue);
        String sValue = editText.getText().toString();

        if(sValue.equals("")) return;
        if(checkIfTagExists("Add", sValue)) return;

        Tag newTag = new Tag(curSpinnerValue, sValue);
        curPhoto.addTag(newTag);

        tagAdapter = new TagAdapter(this, curPhoto.getTags());
        tagAdapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName())==0 ?
                a.getValue().compareToIgnoreCase(b.getValue()) :
                a.getName().compareToIgnoreCase(b.getName()));
        tagNames.setAdapter(tagAdapter);
        myAlbums.writeData(context);

        int newPosition = curPhoto.getTags().indexOf(newTag);
        tagNames.setSelection(newPosition);
        curTag = newTag;
    }

    public Boolean checkIfTagExists(String msgTitle, String newValue) {
//        if(curSpinnerValue.trim().equalsIgnoreCase("location")) {
//            for (Tag cTag : curPhoto.getTags()) {
//                if (cTag.getName().trim().equalsIgnoreCase("location")) {
//                    hc.infoAlert(context, msgTitle, "The location tag can only be used once.");
//                    return true;
//                }
//            }
//        }

        for (Tag cTag : curPhoto.getTags()) {
            if ((cTag.getName().trim().toLowerCase().equals(curSpinnerValue.trim().toLowerCase()))
                    && (cTag.getValue().trim().toLowerCase().equals(newValue.trim().toLowerCase()))) {
                hc.infoAlert(context, msgTitle, "This tag already exists.");
                return true;
            }
        }
        return false;
    }

    public void removeTag(View view) {

//        EditText editText = (EditText) findViewById(R.id.etTagValue);
//        String sValue = editText.getText().toString();
//
//        if(sValue.equals("")) return;
        if (curPhoto.getTags().size() == 0) return;

        if(curTag == null) {
            curTag = (Tag) (tagNames.getItemAtPosition(0));
            tagNames.setSelection(0);
        }

        curPhoto.removeIfTagExists(curTag);

        tagAdapter = new TagAdapter(this, curPhoto.getTags());
        tagAdapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName())==0 ?
                a.getValue().compareToIgnoreCase(b.getValue()) :
                a.getName().compareToIgnoreCase(b.getName()));
        tagNames.setAdapter(tagAdapter);
        tagNames.setSelection(0);
        myAlbums.writeData(context);

        if (curPhoto.getTags().size() > 0) {
            curTag = (Tag) (tagNames.getItemAtPosition(0));
        }
//        else {
//            ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
//            curImage.setImageBitmap(null);
//        }
    }

    public void prevButton(View view) {
        curPhoto = curAlbum.getPrevPhoto(curPhoto);
        //lblPhotoName.setText(curPhoto.getName());
        ImageView curImage = (ImageView) findViewById(R.id.imgPhotoView);
        curImage.setImageBitmap(curPhoto.getBitmap());

        tagAdapter = new TagAdapter(this, curPhoto.getTags());
        tagAdapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName())==0 ?
                a.getValue().compareToIgnoreCase(b.getValue()) :
                a.getName().compareToIgnoreCase(b.getName()));
        tagNames.setAdapter(tagAdapter);

        if(curPhoto.getTags().size() > 0) {
            curTag = (Tag) (tagNames.getItemAtPosition(0));
            tagNames.setSelection(0);
        }
    }

    public void nextButton(View view) {
        curPhoto = curAlbum.getNextPhoto(curPhoto);
//        lblPhotoName.setText(curPhoto.getName());
        ImageView curImage = (ImageView) findViewById(R.id.imgPhotoView);
        curImage.setImageBitmap(curPhoto.getBitmap());

        tagAdapter = new TagAdapter(this, curPhoto.getTags());
        tagAdapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName())==0 ?
                a.getValue().compareToIgnoreCase(b.getValue()) :
                a.getName().compareToIgnoreCase(b.getName()));
        tagNames.setAdapter(tagAdapter);

        if(curPhoto.getTags().size() > 0) {
            curTag = (Tag) (tagNames.getItemAtPosition(0));
            tagNames.setSelection(0);
        }
    }

    private void loadSpinner() {

        List<String> listSpinner = new ArrayList<String>();
//        for (Album cAlbum : myAlbums.albums) {
//            if (!cAlbum.getName().equals(curAlbum.getName())) {
//                listSpinner.add(cAlbum.getName());
//            }
//        }

        listSpinner.add("location");
        listSpinner.add("position");

        Spinner sp1 = (Spinner) findViewById(R.id.spinTag);

        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, listSpinner);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(adp1);

        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
//                Toast.makeText(getBaseContext(), listSpinner.get(position), Toast.LENGTH_SHORT).show();
                curSpinnerValue = listSpinner.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
//                Toast.makeText(getBaseContext(), "NoHo" + listSpinner.get(0), Toast.LENGTH_SHORT).show();
                curSpinnerValue = listSpinner.get(0);
            }
        });

    }

}