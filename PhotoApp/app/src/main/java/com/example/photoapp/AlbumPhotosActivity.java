//Edward Esposito and Marjan Atienza
package com.example.photoapp;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.util.ArrayList;
import java.util.List;

public class AlbumPhotosActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.photoapp.MESSAGE";
    public static final String albumName = "album";
    public static final String photoName = "photo";
    private Helper hc = new Helper();
    public static AllAlbums myAlbums;
    public Album curAlbum;
    final Context context = this;
    public ListView photoNames;
    public PhotoAdapter adapter;
    private int position = 0;
    public Photo curPhoto;
    public ImageView curImage;
    static final int REQUEST_IMAGE_OPEN = 1;
    String curSpinnerValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        myAlbums = AllAlbums.readData(this);
        if(myAlbums == null) {
            myAlbums = new AllAlbums();
        }

        if (myAlbums.albums == null) {
            myAlbums.albums = new ArrayList<Album>();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_photos);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        String passedAlbumName = intent.getStringExtra(MainActivity.albumName);

        if(passedAlbumName == null) {
            curAlbum = myAlbums.albums.get(0);
            message = curAlbum.getName();
        } else {
            curAlbum = myAlbums.getAlbumByName(passedAlbumName);
        }

        TextView textView = findViewById(R.id.tvAlbumPhotos);
        textView.setText(message);

        loadSpinner();

        photoNames = (ListView)findViewById(R.id.lstPhotos);
        photoNames.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        photoNames.setSelection(0);

        adapter = new PhotoAdapter(this, curAlbum.getPhotos());
        adapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
        photoNames.setAdapter(adapter);

        if (curAlbum.getPhotos().size() > 0) {
            curPhoto = (Photo) (photoNames.getItemAtPosition(0));

            ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
            curImage.setImageBitmap(curPhoto.getBitmap());
        }

        photoNames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(curPhoto == null) {
                    curPhoto = (Photo) (photoNames.getItemAtPosition(0));
//                    ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
//                    curImage.setImageBitmap(curPhoto.getBitmap());
                    photoNames.setSelection(0);
                } else {
                    curPhoto = (Photo) (photoNames.getItemAtPosition(position));
                    ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
                    curImage.setImageBitmap(curPhoto.getBitmap());
                }
            }});
    }

    @Override
    public void onBackPressed() {
//        hc.infoAlert(context,"Back","Back Pressed.");
        //goback to MainActivity
        //finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void addPhoto(View view) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("image/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_IMAGE_OPEN);
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_OPEN && resultCode == RESULT_OK) {
            Uri fullPhotoUri = data.getData();

            Bitmap bitmap = null;
            try {
                ParcelFileDescriptor p = getContentResolver().openFileDescriptor(fullPhotoUri, "r");
                FileDescriptor f = p.getFileDescriptor();
                bitmap = BitmapFactory.decodeFileDescriptor(f);
                p.close();
            } catch (Exception exception) {
                exception.printStackTrace();
            }

            String name = fullPhotoUri.getLastPathSegment();
            name = getPhotoNameOnly(name);

            if(checkIfPhotoExists("Add", name)) return;

            Photo newPhoto = new Photo(name, bitmap);
            curAlbum.addPhoto(newPhoto);

            ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
            curImage.setImageBitmap(newPhoto.getBitmap());

            adapter = new PhotoAdapter(this, curAlbum.getPhotos());
            adapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
            photoNames.setAdapter(adapter);
            myAlbums.writeData(context);

            int newPosition = curAlbum.getPhotos().indexOf(newPhoto);
            photoNames.setSelection(newPosition);
            curPhoto = newPhoto;

            //hc.infoAlert(context,"Add","Photo added.");
        }
    }

    public String getPhotoNameOnly(String name) {
        String newString = "";
        String separator ="/";
        int sepPos = name.indexOf(separator);

        if (sepPos == -1) {
            return name;
        }

        newString = name.substring(sepPos + separator.length());
        return newString;
    }

    public Boolean checkIfPhotoExists(String msgTitle,String newPhotoName) {
        for (Photo cPhoto : curAlbum.getPhotos()) {
            if (cPhoto.getName().trim().toLowerCase().equals(newPhotoName.trim().toLowerCase())) {
                hc.infoAlert(context, msgTitle, "This photo already exists.");
                return true;
            }
        }
        return false;
    }

    public void removePhoto(View view) {

        if (curAlbum.getPhotos().size() == 0) return;

        if(curPhoto == null) {
            curPhoto = (Photo) (photoNames.getItemAtPosition(0));
            photoNames.setSelection(0);
        }

        curAlbum.removeIfPhotoExists(curPhoto);
        myAlbums.writeData(context);
        adapter = new PhotoAdapter(this, curAlbum.getPhotos());
        adapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
        photoNames.setSelection(0);
        photoNames.setAdapter(adapter);

        if (curAlbum.getPhotos().size() > 0) {
            curPhoto = (Photo) (photoNames.getItemAtPosition(0));
            ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
            curImage.setImageBitmap(curPhoto.getBitmap());
        } else {
            ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
            curImage.setImageBitmap(null);
        }

    }

    private void loadSpinner() {

        List<String> listSpinner = new ArrayList<String>();
        for (Album cAlbum : myAlbums.albums) {
            if (!cAlbum.getName().equals(curAlbum.getName())) {
                listSpinner.add(cAlbum.getName());
            }
        }

        Spinner sp1 = (Spinner) findViewById(R.id.spinAlbum);

        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, listSpinner);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(adp1);

        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
//                Toast.makeText(getBaseContext(), listSpinner.get(position), Toast.LENGTH_SHORT).show();
                curSpinnerValue = listSpinner.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
//                Toast.makeText(getBaseContext(), "NoHo" + listSpinner.get(0), Toast.LENGTH_SHORT).show();
                curSpinnerValue = listSpinner.get(0);
            }
        });

    }

    public void movePhoto(View view) {
        if (curAlbum.getPhotos().size() == 0) return;
//        Optional<ButtonType> result = hc.msgAlert("Move", "Are you sure you want to move this photo to the selected album?");

        //add photo to selected album and remove photo from current album
        Spinner sp1 = (Spinner) findViewById(R.id.spinAlbum);

        Album moveToAlbum = getMoveToAlbum(curSpinnerValue);
        moveToAlbum.removeIfPhotoExists(curPhoto);
        moveToAlbum.addPhoto(curPhoto);
        curAlbum.removeIfPhotoExists(curPhoto);

        myAlbums.writeData(context);
        adapter = new PhotoAdapter(this, curAlbum.getPhotos());
        adapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
        photoNames.setSelection(0);
        photoNames.setAdapter(adapter);

        if (curAlbum.getPhotos().size() > 0) {
            curPhoto = (Photo) (photoNames.getItemAtPosition(0));
            ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
            curImage.setImageBitmap(curPhoto.getBitmap());
        } else {
            ImageView curImage = (ImageView) findViewById(R.id.imgPhoto);
            curImage.setImageBitmap(null);
        }

//        hc.msgAlert(context,"Move","Photo successfully moved.");


    }

    public Album getMoveToAlbum(String moveAlbumName) {
        for (Album cAlbum : myAlbums.albums) {
            if (cAlbum.getName().trim().toLowerCase().equals(moveAlbumName.trim().toLowerCase())) {
                return cAlbum;
            }
        }
        return null;
    }

    public void photoViewer(View view) {
        if (photoNames.getAdapter().getCount() == 0)  return;

        Intent intent = new Intent(this, PhotoViewerActivity.class);
        intent.putExtra(EXTRA_MESSAGE, "Photo: " + curPhoto.getName());
        intent.putExtra(albumName, curAlbum.getName());

        if(curPhoto == null) {
            curPhoto = (Photo) (photoNames.getItemAtPosition(0));
        }
        intent.putExtra(photoName, curPhoto.getName());

        startActivity(intent);
    }

}