//Edward Esposito and Marjan Atienza
package com.example.photoapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public class SerializablePhoto implements Serializable {
    private static final long serialVersionUID = -1266447099158556616L;
    private byte[] bytes;

    public SerializablePhoto(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        bytes = stream.toByteArray();
    }

    public Bitmap getBitmap() {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }
}