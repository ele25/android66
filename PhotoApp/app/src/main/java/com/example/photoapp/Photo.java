//Edward Esposito and Marjan Atienza
package com.example.photoapp;

import android.graphics.Bitmap;
import java.io.Serializable;
import java.util.ArrayList;

public class Photo implements Serializable {

    private static final long serialVersionUID = 6955723612371190680L;
    private String name = "";
//    public String caption = "";
    private ArrayList<Tag> tags;
    private SerializablePhoto bitmap;

    public Photo(String name, Bitmap bitmap) {
        this.name = name;
        this.tags = new ArrayList<Tag>();
        this.bitmap = new SerializablePhoto(bitmap);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getBitmap() {
        return bitmap.getBitmap();
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }

    public void removeIfTagExists(Tag curTag) {
        if (!tags.isEmpty()) {
            for (Tag tag: tags) {
                if ((curTag.getName().trim().toLowerCase().equals(tag.getName().trim().toLowerCase()))
                        && (curTag.getValue().trim().toLowerCase().equals(tag.getValue().trim().toLowerCase()))) {
                    tags.remove(tag);
                    return;
                }
            }
        }
        return;
    }

//    public Photo(String name, SerializableImage image, Calendar date) {
//        this.name = name;
//        this.caption = "";
//        this.image = image;
//        this.date = date;
//        this.tags = new ArrayList<Tag>();
//        this.date.set(Calendar.MILLISECOND, 0);
//    }


//    public Photo(String name, Image image, Calendar date) {
//        this.name = name;
//        this.caption = "";
////        this.image = new SerializableImage(image);
//        this.date = date;
//        this.tags = new ArrayList<Tag>();
//        this.date.set(Calendar.MILLISECOND, 0);
//    }

//    public String getCaption() {
//        return caption;
//    }
//    public void setCaption(String caption) {
//        this.caption = caption;
//    }

//    public void renameCaption(String rcaption) {
//        this.caption = rcaption;
//    }

//    public Calendar getDate() {
//        return date;
//    }
//
//    public void setDate(Calendar date) {
//        this.date = date;
//    }

//    public Image getImage() {
//        return image.getImage();
//    }

}
