//Edward Esposito and Marjan Atienza
package com.example.photoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.content.Context;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.photoapp.MESSAGE";
    public static final String albumName = "album";
    private Helper hc = new Helper();
    public static AllAlbums myAlbums;
    public Album selAlbum;
    final Context context = this;
    public ListView albumNames;
    public AlbumAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        myAlbums = AllAlbums.readData(this);
        if(myAlbums == null) {
            myAlbums = new AllAlbums();
        }

        if (myAlbums.albums == null) {
            myAlbums.albums = new ArrayList<Album>();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        albumNames = (ListView)findViewById(R.id.lstAlbums);
        albumNames.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        albumNames.setSelection(0);

        adapter = new AlbumAdapter(this, myAlbums.albums);
        adapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
        albumNames.setAdapter(adapter);

        if (myAlbums.albums.size() > 0) {
            selAlbum = (Album) (albumNames.getItemAtPosition(0));
            EditText editText = (EditText) findViewById(R.id.etAlbumName);
            editText.setText(selAlbum.getName().trim());
            TextView editTextNP = (TextView) findViewById(R.id.tvPhotoCount);
            editTextNP.setText(String.valueOf(selAlbum.getPhotos().size()));
        }

        albumNames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                if (position == null) return;
                EditText editText = (EditText) findViewById(R.id.etAlbumName);

                if(selAlbum == null) {
                    selAlbum = (Album) (albumNames.getItemAtPosition(0));
                    editText.setText(selAlbum.getName());
                    albumNames.setSelection(0);
                } else {
                    selAlbum = (Album) (albumNames.getItemAtPosition(position));
                    editText.setText(selAlbum.getName());
                }
                TextView editTextNP = (TextView) findViewById(R.id.tvPhotoCount);
                editTextNP.setText(String.valueOf(selAlbum.getPhotos().size()));
        }});

    }

    @Override
    public void onBackPressed() {
        //exit application
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

     public void openAlbum(View view) {
        if (albumNames.getAdapter().getCount() == 0)  return;

        Intent intent = new Intent(this, AlbumPhotosActivity.class);
        EditText editText = (EditText) findViewById(R.id.etAlbumName);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, "Album: " + message);

         intent.putExtra(albumName, selAlbum.getName());

//        int position = adapter.getPosition(selAlbum);
//        if(position <= 0) {
//            intent.putExtra("curPosition", 0);
//            intent.putExtra(albumName, "");
//        } else {
//            intent.putExtra("curPosition", albumNames.getCheckedItemPosition());
//            intent.putExtra(albumName, selAlbum.getName());
//        }

        startActivity(intent);
    }

    public void searchPhotos(View view) {
        Intent intent = new Intent(this, SearchPhotosActivity.class);
//        EditText editText = (EditText) findViewById(R.id.etAlbumName);
//        String message = editText.getText().toString();
//        intent.putExtra(EXTRA_MESSAGE, "Album: " + message);
        startActivity(intent);
    }

    public void createAlbum(View view) {

        EditText editText = (EditText) findViewById(R.id.etAlbumName);
        String message = editText.getText().toString().trim();

        if(message.equals("")) {
            hc.infoAlert(context,"Create","Please enter an album name.");
            return;
        }

        if(checkIfAlbumExists("Create",message)) return;

        Album newAlbum = new Album(message);
        myAlbums.albums.add(newAlbum);
        myAlbums.writeData(context);
        adapter = new AlbumAdapter(this, myAlbums.albums);
        adapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
        albumNames.setAdapter(adapter);
        TextView editTextNP = (TextView) findViewById(R.id.tvPhotoCount);
        editTextNP.setText("0");

        selAlbum = newAlbum;
        int position = adapter.getPosition(selAlbum);
        albumNames.setSelection(position);
//        albumNames.getChildAt(position).setBackgroundColor(Color.YELLOW);

//        int newPosition = myAlbums.albums.indexOf(newAlbum);
//        albumNames.setSelection(newPosition);
//        selAlbum = newAlbum;

    }

    public Boolean checkIfAlbumExists(String msgTitle,String newAlbumName) {
        for (Album cAlbum : myAlbums.albums) {
            //System.out.println("Album: " +  cAlbum.getName());
            if (cAlbum.getName().trim().toLowerCase().equals(newAlbumName.trim().toLowerCase())) {
                hc.infoAlert(context, msgTitle, "This album already exists.");
                return true;
            }
        }
        return false;
    }

    public void renameAlbum(View view) {

        EditText editText = (EditText) findViewById(R.id.etAlbumName);
        String message = editText.getText().toString().trim();

        if (myAlbums.albums.size() == 0) {
            hc.infoAlert(context, "Rename", "There are no albums in the list to rename.");
            return;
        }

        if (message.equals("")) {
            hc.infoAlert(context,"Rename", "Please select an album to rename.");
            return;
        }

        if(checkIfAlbumExists("Rename", message)) return;

        //hc.msgAlert(context, "Rename", "re you sure you want to rename this album?");
        //if (msgValue == 0)  return;

        if((selAlbum == null) || (myAlbums.albums.size() <= 1)) {
            selAlbum = (Album) (albumNames.getItemAtPosition(0));
        }
        else {
            int position = adapter.getPosition(selAlbum);
            selAlbum = (Album) (albumNames.getItemAtPosition(position));
        }

        selAlbum.renameAlbum(message);
        myAlbums.writeData(context);
        adapter = new AlbumAdapter(this, myAlbums.albums);
        adapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
        albumNames.setAdapter(adapter);
        albumNames.setSelection(0);

    }

    public Album getAlbum(String albumName) {
        for (Album findAlbum : myAlbums.albums) {
            if (findAlbum.getName().equals(albumName))
                return findAlbum;
        }
        return null;
    }

    public void deleteAlbum(View view) {

        EditText editText = (EditText) findViewById(R.id.etAlbumName);
        String message = editText.getText().toString().trim();
        TextView editTextNP = (TextView) findViewById(R.id.tvPhotoCount);

        if (myAlbums.albums.size() == 0) {
            hc.infoAlert(context, "Delete", "There are no albums in the list to delete.");
            return;
        }

        if (message.equals("")) {
            hc.infoAlert(context,"Delete", "Please select an album to delete.");
            return;
        }

        //hc.msgAlert(context, "Delete", "Are you sure you want to delete this album?");
        //if (msgValue == 0)  return;

        myAlbums.albums.remove(selAlbum);
        myAlbums.writeData(context);
        adapter = new AlbumAdapter(this, myAlbums.albums);
        adapter.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
        albumNames.setSelection(0);
        albumNames.setAdapter(adapter);

        if (myAlbums.albums.size() == 0) {
            editText.setText("");
            editTextNP.setText("0");
        } else {
            selAlbum = (Album) (albumNames.getItemAtPosition(0));
            editText.setText(selAlbum.getName());
            editTextNP.setText(String.valueOf(selAlbum.getPhotos().size()));
        }
    }
}