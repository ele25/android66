//Edward Esposito and Marjan Atienza
package com.example.photoapp;

import java.io.Serializable;
import java.text.SimpleDateFormat;
//import java.util.ArrayList;
import java.util.*;

public class Album implements Serializable {

    private static final long serialVersionUID = -1259821160716746676L;
    private String name = "";
    private ArrayList<Photo> photos;

    public Album(String name) {
        this.name = name;
        this.photos = new ArrayList<Photo>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void renameAlbum(String rname) {
        this.name = rname;
    }

    public ArrayList<Photo> getPhotos() {
        return this.photos;
    }

    public void addPhoto(Photo photo) {
        photos.add(photo);
    }

    public int numPhotos() {
        return photos.size();
    }

    public void removeIfPhotoExists(Photo curPhoto) {
        if (!photos.isEmpty()) {
            for (Photo photo: photos) {
                if(curPhoto.getName().equals(photo.getName())) {
                    photos.remove(photo);
                    return;
                }
            }
        }
        return;
    }

    public Photo getNextPhoto(Photo curPhoto) {
        boolean isMatch = false;
        if (photos.isEmpty()) return curPhoto;
        for (Photo photo: photos) {
            if(curPhoto.getName().equals(photo.getName())) {
                isMatch = true;
            }
            if(!curPhoto.getName().equals(photo.getName()) && (isMatch)) {
                return photo;
            }
        }
        return curPhoto;
    }

    public Photo getPrevPhoto(Photo curPhoto) {
        if (photos.isEmpty()) return curPhoto;
        Photo prevPhoto = curPhoto;
        for (Photo photo: photos) {
            if(curPhoto.getName().equals(photo.getName())) {
                return prevPhoto;
            }
            prevPhoto = photo;
        }
        return prevPhoto;
    }

}
