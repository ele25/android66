//Edward Esposito and Marjan Atienza
package com.example.photoapp;

import android.content.Context;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.io.Serializable;
import java.util.ArrayList;

public class AllAlbums implements Serializable {
    private static final long serialVersionUID = -3686332004284259097L;
    public ArrayList<Album> albums;
    private static final String fileName = "albums.dat";

    public static AllAlbums readData(Context context){
        AllAlbums all = null;
//      Toast.makeText(context, "Path"  + context.getFilesDir(),Toast.LENGTH_LONG).show();
        try {

            FileInputStream f = context.openFileInput(fileName);
            ObjectInputStream o = new ObjectInputStream(f);
            all = (AllAlbums) o.readObject();

            if (all.albums == null) {
                all.albums = new ArrayList<Album>();
            }
            f.close();
            o.close();
        } catch (Exception ef) {
            return null;
        }
        return all;
    }

    public void writeData(Context context){
        //ObjectOutputStream oos;
        try {
            FileOutputStream f = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream o = new ObjectOutputStream(f);
            o.writeObject(this);
            o.close();
            f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Album getAlbumByName(String sAlbumName) {
        for (Album cAlbum : albums) {
            if (cAlbum.getName().trim().toLowerCase().equals(sAlbumName.trim().toLowerCase())) {
                return cAlbum;
            }
        }
        return null;
    }

}
